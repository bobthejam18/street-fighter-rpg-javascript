document.addEventListener("DOMContentLoaded", () => {
  /// all variable ///
  let characterContainer = document.querySelector(".character-container");
  let characterHoverAudio = document.querySelector(".character-audio");
  let characterSelectAudio = document.querySelector(".character-select-audio");
  let titleAudio = document.querySelector(".title-audio");
  let loseAudio = document.querySelector(".you-lose");
  let winAudio = document.querySelector(".you-win");
  let characterNameBox = document.querySelector(".character-name-box");
  let titleText = document.querySelector(".title-text");
  let staratPage = document.getElementById('start-page')
  let stagePage = document.getElementById('stage-page')
  let cpuProggress = document.getElementById('cpu-health')
  let playerProggress = document.getElementById('player-health')
  let selectCharacter = document.querySelector('.select-character-container')
  let cpuCharacter = document.querySelector('.cpu-character-container')
  let battleButtons = document.querySelector('.battle-button-container')
  let cpuBattleButtons = document.querySelector('.cpu-battle-button-container')
  let soundContainer = document.querySelector('.sounds')
  let battleAudio = document.querySelector('.battle-audio')
  let modalContainer = document.querySelector('.modal-container')
  function audioSet(setSrc) {
    battleAudio.setAttribute('src',setSrc)
  }
  
  ///attack value ///
  var currentItem = 0
  const ATTACK_VALUE = 8;
  const CPU_ATTACK_VALUE = 9;
  const MAX_LİFE = 100;
  var currentcpuHealth = MAX_LİFE;
  var currentplayerHealth = MAX_LİFE;
  startHealtBar(MAX_LİFE)
  /// object all in one content ///
  let content = 
  {
    characters: [
      { name: "ryu",  id: "ryu",img: "images/ryu.jpg"},
      { name: "ken",  id: "ryu",img: "images/ken.jpeg"},
      { name: "honda",  id: "ryu",img: "images/honda.jpg"},
      { name: "zangief",  id: "ryu",img: "images/zangief.jpg"},
      { name: "blanka",  id: "ryu",img: "images/blanka.jpg"},
    ],
    ryuKenArray: [
      { soundName: "Hadoken", soundSource: "sound/ryuken-hadoken.mp3" },
      { soundName: "Shoryuken", soundSource: "sound/ryuken-shoryuken.mp3" },
      { soundName: "Punch", soundSource: "sound/punch.wav" },
    ],
    blankaArray: [
      { soundName: "BlankaRoll", soundSource: "sound/blanka-growl.mp3" },
      { soundName: "Elektric", soundSource: "sound/blanka-ekektric.wav" },
      { soundName: "Punch", soundSource: "sound/punch.wav" },
    ],
    hondaArray: [
      { soundName: "Torpedo", soundSource: "sound/e-honda-duf-goi.mp3" },
      { soundName: "Hand Slap", soundSource: "sound/punch.wav" },
      { soundName: "Punch", soundSource: "sound/punch.wav" },
    ],
    zangiefArray: [
      { soundName: "Fast Lariat", soundSource: "sound/zangief-grunt.mp3" },
      { soundName: "Banishing", soundSource: "sound/zangief-laugh.mp3" },
      { soundName: "Punch", soundSource: "sound/punch.wav" },
    ],
    stageBackgroundArray: [
      { name: "ryu",ryuBg: "images/ryu-stage.jpg"},
      { name: "ken",kenBg: "images/ken-stage.jpg"},
      { name: "honda",hondaBg: "images/honda-stage.jpg"},
      { name: "zangief",zangiefBg: "images/zangief-stage.jpg"},
      { name: "blanka",blankaBg: "images/blanka-stage.jpg"},
    ],
  }
/// object in array selected ///
  var characterData = content.characters
  var ryuKenSounds = content.ryuKenArray
  var blankaSounds = content.blankaArray
  var hondaSounds = content.hondaArray
  var zangiefSounds = content.zangiefArray
  var stageBackground = content.stageBackgroundArray


  // let ryuKenArray = ["Hadoken","Shoryuken","Punch"];
  // let blankaArray = ["BlankaRoll","Elektric","Punch"];
  // let hondaArray = ["Torpedo","Hand Slap","Punch"];
  // let zangiefArray = ["Fast Lariat","Banishing Flat","Punch"];

  let text = "STREET  FİGHTER  RPG  VERSİON";

  var count = 0;
/// banner text interval letter by letter ///
  function bannerTextAdding() {
    titleText.innerHTML = text.slice(0, count);
    count++;
    if (count > text.length) {
      count = 0;
    }
  }
  setInterval(bannerTextAdding, 150);
////create character and buttons dynamic ///
  characterData.forEach((item) => {
    const character = document.createElement("div");
    characterContainer.appendChild(character);
    character.setAttribute("class", "character");
    character.setAttribute("name", item.name);
    character.setAttribute("id", item.id);
    const buttonElement = document.createElement("button");
    buttonElement.setAttribute("class", "character-button");
    buttonElement.setAttribute("name", item.name);
    character.appendChild(buttonElement);
    const characterImage = document.createElement("img");
    characterImage.setAttribute("class", "character-image");
    characterImage.setAttribute("id", item.name);
    characterImage.setAttribute("src", item.img);
    buttonElement.appendChild(characterImage);
    characterHoverSound();
    characterImage.addEventListener("click",function (e) {
      setBattlePage(item,name)
       setCpuCharacter(characterData)
      $.mobile.changePage('#stage-page')
    })
  });
  /// set player name, image, bg image  ///
  function setBattlePage(item) {
    const itemName = item.name
    selectCharacter.innerHTML = `<img class='battle-img img-fluid img img-responsive' src='${item.img}'>
    <h1 class="battle-name">${item.name}</h1>`
    if (itemName === 'ken') {
      setCaracterArray(ryuKenSounds)
      setStageBackground("images/ken-stage.jpg")
      audioSet("sound/ken.mp3")
    }else if(itemName === 'ryu') {
      setCaracterArray(ryuKenSounds)
      setStageBackground("images/ryu-stage.jpg")
      audioSet("sound/ryu.mp3")
    }else if(itemName === 'blanka') {
      setCaracterArray(blankaSounds)
      setStageBackground("images/blanka-stage.jpg")
      audioSet("sound/blanka.mp3")
    }
    else if(itemName === 'honda') {
      setCaracterArray(hondaSounds)
      setStageBackground("images/honda-stage.jpg")
      audioSet("sound/honda.mp3")
    }
    else if(itemName === 'zangief') {
      setCaracterArray(zangiefSounds)
      setStageBackground("images/zangief-stage.jpg")
      audioSet("sound/zangief.mp3")
    }
  }
/// set cpu name and image ///
  function setCpuCharacter(item) {
    currentItem = Math.floor(Math.random() * item.length)
    const element = item[currentItem]
    const cpuName = element.name
      console.log(element);
      cpuCharacter.innerHTML = `<img class='battle-img img-fluid img img-responsive' src='${element.img}'>
    <h1 class="battle-name">${element.name}</h1>`
    if (cpuName === 'ken') {
      setCpuCaracterArray(ryuKenSounds)
    }else if(cpuName === 'ryu') {
      setCpuCaracterArray(ryuKenSounds)
    }else if(cpuName === 'blanka') {
      setCpuCaracterArray(blankaSounds)
    }
    else if(cpuName === 'honda') {
      setCpuCaracterArray(hondaSounds)
    }
    else if(cpuName === 'zangief') {
      setCpuCaracterArray(zangiefSounds)
    }
  }
///character select mouse over function///
  function characterHoverSound() {
    const charatcerButton = document.querySelectorAll(".character-button");
    charatcerButton.forEach((character) => {
      character.addEventListener("mouseover", function (e) {
        characterHoverAudio.currentTime = 0;
        characterHoverAudio.play();
        characterNameBox.innerHTML = e.currentTarget.name
      });
    });
  }
  ////audio events////
  $("#start-page").on("pageshow", function (event) {
    characterSelectAudio.pause();
    characterSelectAudio.currentTime = 0;
    battleAudio.pause();
    battleAudio.currentTime = 0;
  });
  $("#character-page").on("pageshow", function (event) {
    battleButtons.innerHTML = ""
    battleAudio.pause();
    battleAudio.currentTime = 0;
    characterSelectAudio.play();
    characterSelectAudio.loop = true;
  });
  $("#stage-page").on("pageshow", function (event) {
    characterSelectAudio.pause();
    characterSelectAudio.currentTime = 0;
    battleAudio.play()
    battleAudio.loop = true;
  });
//// main page thunder animation set timeout///
  setTimeout(() => {
    staratPage.classList.add('start-page-animation')
  }, 1000);
////player character create function///
  function setCaracterArray(arrayName) {
    arrayName.forEach(item => {
      const button = document.createElement("button")
      button.setAttribute("class","battle-buttons")
      button.setAttribute("data-inline","true")
      button.innerHTML = item.soundName
      battleButtons.appendChild(button)
      const audio = document.createElement('audio');
     audio.setAttribute('src',item.soundSource)
     audio.setAttribute('id',item.soundName)
     soundContainer.appendChild(audio)
      button.addEventListener("click",function () {
        document.getElementById(item.soundName).play()
        attackHendler ()
    })
    })
  }
  ///cpu create function////
  function setCpuCaracterArray(arrayName) {
    arrayName.forEach(item => {
      const button = document.createElement("button")
      button.setAttribute("class","battle-buttons")
      button.setAttribute("data-inline","true")
      button.innerHTML = item.soundName
      cpuBattleButtons.appendChild(button)
      const audio = document.createElement('audio');
     audio.setAttribute('src',item.soundSource)
     audio.setAttribute('id',item.soundName)
     soundContainer.appendChild(audio)
      button.addEventListener("click",function () {
        document.getElementById(item.soundName).play()
        attackHendler ()
    })
    })
  }
//// attack and hit function////
  function startHealtBar (max) {
    cpuProggress.value = max
    cpuProggress.max = max
    playerProggress.value = max
    playerProggress.max = max
  }

  function setCpuDamage(damage) {
    const setDamage = Math.random() * damage
    cpuProggress.value-=setDamage
    return setDamage
  }
  function setplayerDamage(damage) {
    const setDamage = Math.random() * damage
    playerProggress.value-=setDamage
    return setDamage
  }
  function attackHendler () {
    const cpuDamage = setCpuDamage(ATTACK_VALUE);
    currentcpuHealth -= cpuDamage;
    const playerDamage = setplayerDamage(CPU_ATTACK_VALUE);
    currentplayerHealth -= playerDamage;
    if (currentcpuHealth > 0 && currentplayerHealth <= 0) {
      loseAudio.play()
      $.mobile.changePage('#character-page')
      alert("you lose")
      currentcpuHealth=MAX_LİFE;
      currentplayerHealth=MAX_LİFE
    }
    else if (currentcpuHealth <= 0 && currentplayerHealth > 0) {
      winAudio.play()
      $.mobile.changePage('#character-page')
      alert("you won")
      currentcpuHealth=MAX_LİFE;
      currentplayerHealth=MAX_LİFE
    }else if (currentcpuHealth <= 0 && currentplayerHealth <= 0) {
      alert('You have a draw!');
      currentcpuHealth=MAX_LİFE;
      currentplayerHealth=MAX_LİFE
      $.mobile.changePage('#character-page')
    }
  }
///set background function/////
  function setStageBackground(imgSrc) {
    stagePage.style.backgroundImage  ="url("+imgSrc+")"
  }
  $('#stage-page').on("pagebeforeshow", function(){
    startHealtBar(MAX_LİFE)
  });
/// pausse menu ///
  let modal = `<div data-role="popup" id="pauseMenu" data-overlay-theme="b" data-theme="b" data-dismissible="false" style="max-width:400px;">
      <div data-role="header" data-theme="a">
            <h1>Pause Menu</h1>
      </div>
      <div role="main" class="ui-content">
          <h3 class="ui-title pause-text">If you leave the page, your current situation will change.</p>
          <a href="#" class="ui-btn ui-corner-all ui-shadow ui-btn-block ui-btn-b"        data-rel="back">Cancel</a>
          <a href="#character-page" data-role="button" data-inline="false">Character Select</a>
          <a href="#start-page" data-role="button" data-inline="false">Home Page</a>
      </div>
  </div>`;
modalContainer.innerHTML = modal
});
